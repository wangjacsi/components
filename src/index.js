import React from 'react';
import ReactDOM from 'react-dom';
import faker from 'faker';

import CommentDetail from './CommentDetail'
import ApprovalCard from './ApprovalCard'

const App = () => {
    return (
        <div className="ui container comments">
            <ApprovalCard>
                <CommentDetail 
                    author="Sam" 
                    timeAgo="Today at 4:45PM" 
                    content="Nice blog Post!!" 
                    avatar={faker.image.avatar()}
                />
            </ApprovalCard>
            
            <ApprovalCard>
                <CommentDetail 
                    author="Alex" 
                    timeAgo="Today at 2:45AM" 
                    content="Who am I and You know!" 
                    avatar={faker.image.avatar()}
                />
            </ApprovalCard>

            <ApprovalCard>
                <CommentDetail 
                    author="Sean" 
                    timeAgo="Yesterday at 6:45PM" 
                    content="NodeJs is great and React also!" 
                    avatar={faker.image.avatar()}
                />
            </ApprovalCard>

           
            <ApprovalCard>
                <CommentDetail 
                    author="Jane" 
                    timeAgo="Yesterday at 4:45PM" 
                    content="Mymison Loves you"
                    avatar={faker.image.avatar()}
                />
            </ApprovalCard>
            
            
        </div>
    );
};

ReactDOM.render(<App />, document.querySelector('#root'));

